#include <stdio.h>
#include <conio.h>
#include "App.h"


__device__ void get_g(int rn, int pn, float *g, float *d_LR)
{
    //if(threadIdx.y == 0)
    {
	    if(rn<0 || pn<0 || rn>=n/l ||pn>=n/l)
            g[threadIdx.x] = 0;
        else
        {
            //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004Inside getg\n"); getch();}
            g[threadIdx.x] = d_LR[(n/l)*(n/l)*threadIdx.x+(n/l)*rn+pn];
            //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004Inside getg after copy\n g[0] = %f\n", g[0]); getch();}
        }
        //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004Inside getg outside else\n g[0] = %f\n", g[0]); getch();}
    }
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004Inside getg after if threadIdx.y == 0\n g[0] = %f\n", g[0]); getch();}
    __syncthreads();
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004Inside getg after syncthreads\n g[0] = %f\n", g[0]); getch();}

}

/*__device__ void MatMul(float *mat1, float *mat2, float *res)
{
	res[threadIdx.x] = 0;
	__syncthreads();
	res[threadIdx.x] += mat1[(l*l) * threadIdx.x + threadIdx.y]*mat2[threadIdx.y];
	__syncthreads();	
}*/

__device__ void MatrixMul1(float *mat1, float *mat2, float *res)
{
	if(threadIdx.y == 0)
	{
		float sum = 0.0;
		for(int i = 0; i<(l*l); i++)
		{
			sum += mat1[(l*l)*threadIdx.x + i]*mat2[i]; 
		}
		res[threadIdx.x] = sum;
	}
	__syncthreads();
}


__device__ void MatSub(float *mat1, float *mat2)
{
	if(threadIdx.y == 0)
		mat1[threadIdx.x] = mat1[threadIdx.x] - mat2[threadIdx.x];
	__syncthreads();

}

__device__ void MatAdd(float *mat1, float *mat2)
{
	if(threadIdx.y == 0)
		mat1[threadIdx.x] = mat1[threadIdx.x] + mat2[threadIdx.x];
	__syncthreads();

}

__device__ void flush(float *temp1)
{
	if(threadIdx.y == 0)
		temp1[threadIdx.x] = 0;
	__syncthreads();
}

__device__ void get_gCap(int rn, int pn, float *gCap, float *d_LR, float *d_AT)
{
        
    __shared__ float g[(l*l)];
    __shared__ float temp[(l*l)];

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP000\n"); getch();}
    get_g(rn-1,pn-1,g,d_LR);
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP AFTER GETG\n"); getch();}
    MatrixMul1(&d_AT[(l*l)*(l*l)*8],g,temp);
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP AFTER MATMUL\n"); getch();}
    MatAdd(gCap,temp);
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP AFTER MATADD\n"); getch();}//performs gCap = gCap + temp
    flush(temp);
    __syncthreads();
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP AFTER FLUSH\n"); getch();}

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP001\n"); getch();}
    get_g(rn,pn-1,g,d_LR);
    MatrixMul1(&d_AT[(l*l)*(l*l)*7],g,temp);
    MatAdd(gCap,temp);
    flush(temp);
    __syncthreads();
    
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP002\n"); getch();}
    get_g(rn+1,pn-1,g,d_LR);
    MatrixMul1(&d_AT[(l*l)*(l*l)*6],g,temp);
    MatAdd(gCap,temp);
    flush(temp);
    __syncthreads();

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP003\n"); getch();}
    get_g(rn-1,pn,g,d_LR);
    MatrixMul1(&d_AT[(l*l)*(l*l)*5],g,temp);
    MatAdd(gCap,temp);
    flush(temp);
    __syncthreads();

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004\n"); getch();}
    get_g(rn,pn,g,d_LR);
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004after getg\n"); getch();}
    MatrixMul1(&d_AT[(l*l)*(l*l)*4],g,temp);
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004after matmul\n"); getch();}
    MatAdd(gCap,temp);
    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP004after matadd\n"); getch();}
    flush(temp);
    __syncthreads();

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP005\n"); getch();}
    get_g(rn+1,pn,g,d_LR);
    MatrixMul1(&d_AT[(l*l)*(l*l)*3],g,temp);
    MatAdd(gCap,temp);
    flush(temp);
    __syncthreads();

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP006\n"); getch();}
    get_g(rn-1,pn+1,g,d_LR);
    MatrixMul1(&d_AT[(l*l)*(l*l)*2],g,temp);
    MatAdd(gCap,temp);
    flush(temp);
    __syncthreads();

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP007\n"); getch();}
    get_g(rn,pn+1,g,d_LR);
    MatrixMul1(&d_AT[(l*l)*(l*l)*1],g,temp);
    MatAdd(gCap,temp);
    flush(temp);
    __syncthreads();

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP008\n"); getch();}
    get_g(rn+1,pn+1,g,d_LR);
    MatrixMul1(&d_AT[(l*l)*(l*l)*0],g,temp);
    MatAdd(gCap,temp);
    flush(temp);
    __syncthreads();

    //if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0) {printf("I'M INSIDE GCAP009\n"); getch();}
    /*if(threadIdx.x == 0 && threadIdx.y == 0 && rn == 0 && pn == 0)
    {
        
        for(int i =0; i<(l*l); i++)
            printf("%f    ", gCap[i]);
        getch();
    }*/
    __syncthreads();
}

__device__ void remove_kachra(int rn, int pn, float *gCap, float *temp1, float *d_A, float *d_f)
{

    MatrixMul1(&d_A[(l*l)*(l*l)*0], &d_f[((n/l+2)*(l*l))*(rn-1) + (l*l)*(pn-1)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();

    MatrixMul1(&d_A[(l*l)*(l*l)*1], &d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn-1)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();

    MatrixMul1(&d_A[(l*l)*(l*l)*2], &d_f[((n/l+2)*(l*l))*(rn+1) + (l*l)*(pn-1)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();
    
    MatrixMul1(&d_A[(l*l)*(l*l)*3], &d_f[((n/l+2)*(l*l))*(rn-1) + (l*l)*(pn)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();

    MatrixMul1(&d_A[(l*l)*(l*l)*5], &d_f[((n/l+2)*(l*l))*(rn+1) + (l*l)*(pn)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();
    
    MatrixMul1(&d_A[(l*l)*(l*l)*6], &d_f[((n/l+2)*(l*l))*(rn-1) + (l*l)*(pn+1)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();

	MatrixMul1(&d_A[(l*l)*(l*l)*7], &d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn+1)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();

	MatrixMul1(&d_A[(l*l)*(l*l)*8], &d_f[((n/l+2)*(l*l))*(rn+1) + (l*l)*(pn+1)],temp1);
	MatSub(gCap,temp1);
	flush(temp1);
	__syncthreads();	
}

__device__ void Gauss(int rn, int pn, float *gCap, float *temp, float *d_A, float *d_f)
{
	
	__shared__ int cnt;
	__shared__ int flag;
	__shared__ float A[(l*l)][(l*l)];
	flag = 0;
	cnt = 0;

    //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS\n"); getch();}

	A[threadIdx.y][threadIdx.x] = d_A[(l*l)*threadIdx.y + threadIdx.x];
	__syncthreads();
	
	if(threadIdx.y == 0)
	gCap[threadIdx.x] = gCap[threadIdx.x]/A[threadIdx.x][threadIdx.x];
	__syncthreads();
	
	if(threadIdx.y != threadIdx.x)
		A[threadIdx.y][threadIdx.x] = (float) A[threadIdx.y][threadIdx.x]/(float)A[threadIdx.y][threadIdx.y];
	__syncthreads();

    //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS before while\n"); getch();}			
	while(1)                   //(cnt < 25)
	{
		
		flag = 0;
        //__syncthreads();
        //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS Inside while  %d\n", cnt); getch();}
		
        //copy old f
        if(threadIdx.y == 0)
		{
			temp[threadIdx.x] = d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn) + threadIdx.x];
			if(threadIdx.x == 0) cnt++;
		}
        //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS Inside while inside old copy f  %d\n", cnt); getch();}
		__syncthreads();
			
		// solve for new value of f
		if(threadIdx.x == 0 && threadIdx.y == 0)
		{
			int i,j;
            
            //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS inside while inside if\n"); getch();}
			for(i = 0; i< (l*l); i++)
			{
				d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn) + i] = gCap[i];
				for(j = 0; j<(l*l); j++)
					if(i!=j)
					d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn) + i] = 
					d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn) + i] - 
						A[i] [j]*d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn) + j];
			}		
		}
        //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS inside while outside if\n"); getch();}
		__syncthreads();
		

		// compare
		if(threadIdx.y == 0)
		{
			//if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS inside while inside compare\n"); getch();}
            if(temp[threadIdx.x] != d_f[((n/l+2)*(l*l))*(rn) + (l*l)*(pn) + threadIdx.x])
				flag = 1;
		}
        //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS inside while outside compare\n"); getch();}
		__syncthreads();
		
		if(flag == 0 || cnt >= 25)
        {
            //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS inside while inside flag == 0\n"); getch();}
            break;
        }
		__syncthreads();
	}
    //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE inside GAUSS after while\n"); getch();}
	__syncthreads();
}


__global__ void kernel_SolveOddRowOddPixel(float *d_LR, float *d_A, float *d_f, float *d_AT)
{

	int rn = 2*blockIdx.y + 1;
	int pn = 2*blockIdx.x + 1;
	

	__shared__ float gCap[(l*l)];
	__shared__ float temp[(l*l)];
    
		
	if(rn != 0 && pn != 0 && rn != (int) (n/l + 1) && pn != (int) (n/l + 1))
	{	
        if(threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K1 BEFORE GCAP  %d   %d \n", rn, pn); }
        get_gCap(rn-1, pn-1, gCap, d_LR, d_AT);
		__syncthreads();
        //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE BEFORE REMOVE_KACHRA\n"); getch();}
		remove_kachra(rn, pn, gCap, temp, d_A, d_f);
		__syncthreads();
        //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE BEFORE GAUSS\n"); getch();}
		Gauss(rn, pn, gCap, temp, d_A, d_f);
        //if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M HERE AFTER GAUSS\n"); getch();}
	}
    if(rn == 1 && pn == 1 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K1 AT THE END OF THE KERNEL\n"); getch();}
}

__global__ void kernel_SolveOddRowEvenPixel(float *d_LR, float *d_A, float *d_f, float *d_AT)
{

	int rn = 2*blockIdx.y + 1;
	int pn = 2*blockIdx.x;
	

	__shared__ float gCap[(l*l)];
	__shared__ float temp[(l*l)];

    if(threadIdx.y == 0)
        gCap[threadIdx.x] = 0;
    __syncthreads();

	if(rn != 0 && pn != 0 && rn != (int) (n/l + 1) && pn != (int) (n/l + 1))
	{	
        if(threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K1 BEFORE GCAP  %d   %d \n", rn, pn); }
		get_gCap(rn-1, pn-1, gCap, d_LR, d_AT);
		__syncthreads();
		remove_kachra(rn, pn, gCap, temp, d_A, d_f);
		__syncthreads();
		Gauss(rn, pn, gCap, temp, d_A, d_f);

	}
    if(rn == 1 && pn == 2 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K2 AT THE END OF THE KERNEL\n"); getch();}
}

__global__ void kernel_SolveEvenRowOddPixel(float *d_LR, float *d_A, float *d_f, float *d_AT)
{

	int rn = 2*blockIdx.y;
	int pn = 2*blockIdx.x + 1;
	

	__shared__ float gCap[(l*l)];
	__shared__ float temp[(l*l)];

	if(rn != 0 && pn != 0 && rn != (int) (n/l + 1) && pn != (int) (n/l + 1))
	{	
		if(threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K1 BEFORE GCAP  %d   %d \n", rn, pn); }
        get_gCap(rn-1, pn-1, gCap, d_LR, d_AT);
		__syncthreads();
		remove_kachra(rn, pn, gCap, temp, d_A, d_f);
		__syncthreads();
		Gauss(rn, pn, gCap, temp, d_A, d_f);

	}
    if(rn == 1 && pn == 2 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K2 AT THE END OF THE KERNEL\n"); getch();}
}

__global__ void kernel_SolveEvenRowEvenPixel(float *d_LR, float *d_A, float *d_f, float *d_AT)
{

	int rn = 2*blockIdx.y;
	int pn = 2*blockIdx.x;
	

	__shared__ float gCap[(l*l)];
	__shared__ float temp[(l*l)];

	if(rn != 0 && pn != 0 && rn != (int) (n/l + 1) && pn != (int) (n/l + 1))
	{	
		if(threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K1 BEFORE GCAP  %d   %d \n", rn, pn); }
        get_gCap(rn-1, pn-1, gCap, d_LR, d_AT);
		__syncthreads();
		remove_kachra(rn, pn, gCap, temp, d_A, d_f);
		__syncthreads();
		Gauss(rn, pn, gCap, temp, d_A, d_f);

	}
    if(rn == 1 && pn == 2 && threadIdx.y == 0 && threadIdx.x == 0) {printf("I'M IN K2 AT THE END OF THE KERNEL\n"); getch();}
}


extern "C" 
void SRREngine(float *d_LR, float *d_f, float *h_f, float *d_A, float *d_AT)
{
    int count = 0, flag =0;    
    // Local Data Declaration ...
    float *t_f, *h_Atemp;
    t_f     = (float *) calloc((n+2*l)*(n+2*l), sizeof(float));
	h_Atemp     = (float *) calloc(9*(l*l)*(l*l),sizeof(float));

    // Apply Regularization Term ...
    // Can be done by launching separate Kernel or in GetSystemMatrices Kernel only ...
   	cudaMemcpy(h_Atemp, d_A, 9*(l*l)*(l*l)*sizeof(float), cudaMemcpyDeviceToHost);
    for(int i=0; i<(l*l); i++)
        for(int j=0; j<(l*l); j++)
        {
            if(i==j){h_Atemp[(l*l)*(l*l)*4 + (l*l)*i + j] += (float)ALPHA;}
        }
    cudaMemcpy(d_A, h_Atemp, 9*(l*l)*(l*l)*sizeof(float), cudaMemcpyHostToDevice);

   	// Blocks and Threads ...
    int blk = (int)n/(2*l)+1;
    dim3 blocks(blk, blk);
	dim3 threads((l*l), (l*l));

    // Gauss-Seidel
    while(count < 50)
    {
        flag = 0;
        // Copy Old h_f to t_f
        for(int i = 0; i<n/l+2; i++)
  		    for(int j = 0; j<n/l+2; j++)
  		    	for(int k = 0; k<(l*l); k++)
      				t_f[((n/l+2)*(l*l))*i+(l*l)*j+k] = h_f[((n/l+2)*(l*l))*i+(l*l)*j+k];

        // Solve .. Launch Kernels ...
        kernel_SolveOddRowOddPixel<<<blocks,threads>>>(d_LR, d_A, d_f, d_AT);
        printf("I'M AFTER K1\n");
        getch();
   		cudaThreadSynchronize();
        printf("I'M BEFORE K2\n");
        getch();
		kernel_SolveOddRowEvenPixel<<<blocks,threads>>>(d_LR, d_A, d_f, d_AT);
        printf("I'M AFTER K2\n");
        getch();
		cudaThreadSynchronize();
		kernel_SolveEvenRowOddPixel<<<blocks,threads>>>(d_LR, d_A, d_f, d_AT);
		cudaThreadSynchronize();
		kernel_SolveEvenRowEvenPixel<<<blocks,threads>>>(d_LR, d_A, d_f, d_AT);
		cudaThreadSynchronize();

		// Copy device f to host f
		cudaMemcpy(h_f, d_f, (n+2*l)*(n+2*l)*sizeof(float), cudaMemcpyDeviceToHost);

        // Compare ... h_f and t_f
        for(int i = 0; i<n/l+2; i++)
        {
            for(int j = 0; j<n/l+2; j++)
            {
                for(int k = 0; k<l*l; k++)
                {
                    if(t_f[((n/l+2)*(l*l))*i+(l*l)*j+k] != h_f[((n/l+2)*(l*l))*i+(l*l)*j+k])
                    {
                        flag =1;
                        printf("%d, %d  ",i,j);
                        printf("%f  =  %f", t_f[((n/l+2)*(l*l))*i+(l*l)*j+k], h_f[((n/l+2)*(l*l))*i+(l*l)*j+k]);
                        getch();
                        break;
                    }
                }
                if(flag == 1) break;
            }
            if(flag == 1) break;
        }
        if(flag == 0) break;
		count++;
    }

    // free Local Data memory ...
     free(h_Atemp);
    free(t_f);
}