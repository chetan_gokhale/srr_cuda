#include <stdio.h>
#include <conio.h>
#include <stdlib.h>

extern "C" 
void LoadYUVImage(float *dst, const char* path, int x_dim, int y_dim)
{
	//Loads Y component of a YUV image to a specified one dimensional array ...
    FILE *ref;

	ref = fopen( path, "rb" );
	if(ref == NULL)
    {
        printf("CANNOT OPEN IMAGE ..\n");
        getch();
        return;
    }

	//fread(dst, sizeof(float), x_dim*y_dim, ref);
    
    for(int i = 0;i<y_dim;i++)
        for(int j = 0;j<x_dim;j++)
            dst[(x_dim)*i + j] = fgetc(ref);

    /*for(int i=0; i<(y_dim)*(x_dim); i++)
        printf("%0.2f     ",dst[i]);
    getch();*/
    
    fclose(ref);
}


extern "C"
void StoreYUVImage(unsigned char *src, const char* path, int x_dim, int y_dim)
{
	FILE *ref;
	
    unsigned char ch = 128;
	ref = fopen( path, "w+" );
	if(ref == NULL)
    {
        printf("CANNOT OPEN IMAGE ..\n");
        getch();
        return;
    }

	//fwrite(src, sizeof(unsigned char), x_dim*y_dim, ref);
    for(int i = 0;i<y_dim;i++)
        for(int j = 0;j<x_dim;j++)
            fputc((int)(src[(x_dim)*i + j] + 0.5),ref);
            
	/***???***/
	for(int i = 0; i<x_dim*y_dim/2; i++)
		fputc(ch,ref);
	
	fclose(ref);
}