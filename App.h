#ifndef APP_H
#define APP_H

/* HR image Size and decimation Factor ...*/
#define         n           192
#define         l             4
#define       ALPHA           0.3


/*Function Declarations ...*/

// Image Handling Functions
extern "C" void LoadYUVImage(float *dst, const char* path, int x_dim, int y_dim);
extern "C" void StoreYUVImage(unsigned char *src, const char* path, int x_dim, int y_dim);

// Application Functions
extern "C" void GetMotionVectors(float *h_mv);         //(d_mv, d_LR)
extern "C" void GetSystemMatrices(float *d_mv, float *d_A, float *d_AT);
extern "C" void SRREngine(float *d_LR, float *d_f, float *h_f, float *d_A, float *d_AT);

#endif