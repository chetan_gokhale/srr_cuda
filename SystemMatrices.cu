#include "App.h"
#include <math.h>

__device__ int calc(int i,int j)
{
    int k;
    if(j<0) {k = ((-j/l + 1)*l+j)%l;}
    else {k = j%l;}
    return(i*l + floor((float)j/l)*n*l + k);
}

__device__ void CalcShiftMat(float x, float y, float *S)
{
	float dx,dy;
    
	dx = x * l;
    dy = y * l;

	float dxCap, dyCap;
	dxCap = floor(dx);
	dyCap = floor(dy);

	float dxBar, dyBar;
	dxBar = dx - dxCap;
	dyBar = dy - dyCap;

	float w1, w2, w3, w4;
	w1 = (1-dxBar)*(1-dyBar);
	w2 = dxBar * (1-dyBar);
	w3 = (1-dxBar) * dyBar;
	w4 = dxBar * dyBar;
	
	__syncthreads();

	int i = threadIdx.x/l;
	int j = threadIdx.x%l;
	int k = threadIdx.x;	
	
	int pij = calc(i+dyCap,j+dxCap);

	if(pij<-n*l)            {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l+l*l] = w1;} //lower lower
     	else if(pij<-n*l+l*l)
     	{   
     		if (y<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l] = w1;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l] = w1;}                            //lower upper or lower lower
     	}
     	else if(pij<-n*l+2*l*l) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l-l*l] = w1;}       //lower upper
     	else if(pij<0)
     	{   
     		if(x<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij+l*l] = w1;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+l*l] = w1;}//upper lower or lower lower
     	}
     	else if(pij<l*l)
     	{
     		if(x<0 && y<0)          {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij] = w1;}               //Can be all four
     		else if(x<0 && y>=0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij] = w1;}
     		else if(x>=0 && y<0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij] = w1;}
     		else                    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij] = w1;}
     	}
     	else if(pij<2*l*l)
     	{
    		if(x<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-l*l] = w1;}           //lower upper or upper upper
     		else   {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij-l*l] = w1;}
     	}
     	else if(pij<n*l)        {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l+l*l] = w1;}    //upper lower
     	else if(pij<(n*l+l*l))
     	{
     		if(y<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-n*l] = w1;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l] = w1;}             //upper lower or upper upper
     	}           
     	else if(pij<(n*l+2*l*l)){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-(n*l+l*l)] = w1;}     //upper upper

	/*********************************************************************************************************************************/

	//Inverted
	pij = calc(i+dyCap+1,j+dxCap);

	if(pij<-n*l)            {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l+l*l] = w2;} //lower lower
     	else if(pij<-n*l+l*l)
     	{   
     		if (y<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l] = w2;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l] = w2;}                            //lower upper or lower lower
     	}
     	else if(pij<-n*l+2*l*l) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l-l*l] = w2;}       //lower upper
     	else if(pij<0)
     	{   
     		if(x<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij+l*l] = w2;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+l*l] = w2;}//upper lower or lower lower
     	}
     	else if(pij<l*l)
     	{
     		if(x<0 && y<0)          {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij] = w2;}               //Can be all four
     		else if(x<0 && y>=0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij] = w2;}
     		else if(x>=0 && y<0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij] = w2;}
     		else                    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij] = w2;}
     	}
     	else if(pij<2*l*l)
     	{
    		if(x<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-l*l] = w2;}           //lower upper or upper upper
     		else   {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij-l*l] = w2;}
     	}
     	else if(pij<n*l)        {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l+l*l] = w2;}    //upper lower
     	else if(pij<(n*l+l*l))
     	{
     		if(y<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-n*l] = w2;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l] = w2;}             //upper lower or upper upper
     	}           
     	else if(pij<(n*l+2*l*l)){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-(n*l+l*l)] = w2;}     //upper upper

	/*******************************************************************************************************************************/

	//Inverted
	pij = calc(i+dyCap,j+dxCap+1);

	if(pij<-n*l)            {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l+l*l] = w3;} //lower lower
     	else if(pij<-n*l+l*l)
     	{   
     		if (y<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l] = w3;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l] = w3;}                            //lower upper or lower lower
     	}
     	else if(pij<-n*l+2*l*l) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l-l*l] = w3;}       //lower upper
     	else if(pij<0)
     	{   
     		if(x<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij+l*l] = w3;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+l*l] = w3;}//upper lower or lower lower
     	}
     	else if(pij<l*l)
     	{
     		if(x<0 && y<0)          {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij] = w3;}               //Can be all four
     		else if(x<0 && y>=0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij] = w3;}
     		else if(x>=0 && y<0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij] = w3;}
     		else                    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij] = w3;}
     	}
     	else if(pij<2*l*l)
     	{
    		if(x<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-l*l] = w3;}           //lower upper or upper upper
     		else   {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij-l*l] = w3;}
     	}
     	else if(pij<n*l)        {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l+l*l] = w3;}    //upper lower
     	else if(pij<(n*l+l*l))
     	{
     		if(y<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-n*l] = w3;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l] = w3;}             //upper lower or upper upper
     	}           
     	else if(pij<(n*l+2*l*l)){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-(n*l+l*l)] = w3;}     //upper upper

	/*********************************************************************************************************************************/
	
	pij = calc(i+dyCap+1, j+dxCap+1);

	if(pij<-n*l)            {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l+l*l] = w4;} //lower lower
     	else if(pij<-n*l+l*l)
     	{   
     		if (y<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l] = w4;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+n*l] = w4;}                            //lower upper or lower lower
     	}
     	else if(pij<-n*l+2*l*l) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij+n*l-l*l] = w4;}       //lower upper
     	else if(pij<0)
     	{   
     		if(x<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij+l*l] = w4;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij+l*l] = w4;}//upper lower or lower lower
     	}
     	else if(pij<l*l)
     	{
     		if(x<0 && y<0)          {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij] = w4;}               //Can be all four
     		else if(x<0 && y>=0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij] = w4;}
     		else if(x>=0 && y<0)    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij] = w4;}
     		else                    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*0 + (l*l)*k + pij] = w4;}
     	}
     	else if(pij<2*l*l)
     	{
    		if(x<0){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-l*l] = w4;}           //lower upper or upper upper
     		else   {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*1 + (l*l)*k + pij-l*l] = w4;}
     	}
     	else if(pij<n*l)        {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l+l*l] = w4;}    //upper lower
     	else if(pij<(n*l+l*l))
     	{
     		if(y<0) {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-n*l] = w4;}
     		else    {S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*2 + (l*l)*k + pij-n*l] = w4;}             //upper lower or upper upper
     	}           
     	else if(pij<(n*l+2*l*l)){S[(4*(l*l)*(l*l))*threadIdx.y + (l*l)*(l*l)*3 + (l*l)*k + pij-(n*l+l*l)] = w4;}     //upper upper


}

__global__ void kernel_GetShiftMatrices(float *d_D, float *d_D1, float *d_S, float *d_ST, float *d_mv)
{
	d_D1[(l*l)*threadIdx.y+threadIdx.x] = (float)1/(l*l*l*l);
	if(threadIdx.x == 0)
		d_D[threadIdx.y] = (float)1/(l*l);
	__syncthreads();

	CalcShiftMat(d_mv[2*threadIdx.y],d_mv[2*threadIdx.y+1], d_S);
	__syncthreads();

	for(int i = 0;i<l*l;i++)
	{
		for(int j=0;j<4;j++)
			d_ST[(4*(l*l)*(l*l))*i + (l*l)*(l*l)*j + (l*l)*threadIdx.y + threadIdx.x] = 
				d_S[(4*(l*l)*(l*l))*i + (l*l)*(l*l)*j + (l*l)*threadIdx.x + threadIdx.y];
	}
	__syncthreads();
}

__global__ void kernel_MatrixMulti(float *d_D1, float *d_S, float *d_ST, float *d_tempStorage)
{
	__shared__ float temp[(l*l)][(l*l)];
	__shared__ float temp1[(l*l)][(l*l)];
	
	//Matrix Multiply D1 by S[blockIdx.y][blockIdx.x%4] and store the result in temp array ...
	float sum = 0;
	for(int i = 0; i<(l*l); i++)
		sum += d_D1[(l*l)*threadIdx.y+i]*d_S[(4*(l*l)*(l*l))*blockIdx.y + (l*l)*(l*l)*(blockIdx.x%4) + (l*l)*i + threadIdx.x];
	temp[threadIdx.y][threadIdx.x] = sum;
	__syncthreads();
	
	//Matrix Multiply ST[blockIdx.y][blockIdx.x/4], temp to temp1
	sum = 0;
	for(int i = 0; i<(l*l); i++)
		sum += d_ST[(4*(l*l)*(l*l))*blockIdx.y + (l*l)*(l*l)*(blockIdx.x/4) + (l*l)*threadIdx.y + i]*temp[i][threadIdx.x];
	temp1[threadIdx.y][threadIdx.x] = sum;
	__syncthreads();

	//Copy temp1 to a global array tempStorage[(l*l)][16][(l*l)][(l*l)]
	d_tempStorage[(16*(l*l)*(l*l))*blockIdx.y + (l*l)*(l*l)*blockIdx.x + (l*l)*threadIdx.y + threadIdx.x] = 
			temp1[threadIdx.y][threadIdx.x];
	__syncthreads();
	
}

__global__ void kernel_GetAMatrices(float *tempStorage, float *A)
{
	// Create 9 A matrices from (l*l)*16 shift matrices each of size (l*l)*(l*l)...
	float sum = 0;
	
	//Abar1bar1
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*12 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*0 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();
	
	//Abar10
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*13 + (l*l)*threadIdx.y + threadIdx.x] + 
				tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*8 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*1 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

	//Abar11
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*9 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*2 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

	//A0bar1
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*4 + (l*l)*threadIdx.y + threadIdx.x] + 
				tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*14 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*3 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

	//A00
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*0 + (l*l)*threadIdx.y + threadIdx.x] + 
			 tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*5 + (l*l)*threadIdx.y + threadIdx.x] + 
				tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*10 + (l*l)*threadIdx.y + threadIdx.x] + 
				tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*15 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*4 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

	//A01
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*1 + (l*l)*threadIdx.y + threadIdx.x] + 
				tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*11 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*5 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

	//A1bar1
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*6 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*6 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

	//A10
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*2 + (l*l)*threadIdx.y + threadIdx.x] + 
				tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*7 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*7 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

	//A11
	sum = 0;
	for(int i=0; i<(l*l); i++)
		sum += tempStorage[(16*(l*l)*(l*l))*i + (l*l)*(l*l)*3 + (l*l)*threadIdx.y + threadIdx.x]; 
	A[(l*l)*(l*l)*8 + (l*l)*threadIdx.y + threadIdx.x] = sum;
	__syncthreads();

}


__device__ void MatMul(float *mat1, float *mat2, float *res)
{
	//if(threadIdx.y == 0)
	{
		float sum = 0.0;
		for(int i = 0; i<(l*l); i++)
		{
			sum += mat1[(l*l)*threadIdx.x + i]*mat2[i]; 
		}
		res[threadIdx.x] = sum;
	}
	__syncthreads();
}


__global__ void kernel_GetATMatrices(float *ST, float *D, float *AT, float *d_mv)
{
	
	__shared__ float temp[(l*l)];
		
	if(d_mv[2*blockIdx.y]<0 && d_mv[2*blockIdx.y+1]<0)
	{
		MatMul(&ST[(4*(l*l)*(l*l))*blockIdx.y + (l*l)*(l*l)*blockIdx.x], D, temp);
		AT[(l*l)*(l*l)*(blockIdx.x + (blockIdx.x/2)) + (l*l)*threadIdx.x + blockIdx.y] = temp[threadIdx.x];
		__syncthreads();
	}

	else if(d_mv[2*blockIdx.y]<0 && d_mv[2*blockIdx.y+1]>=0)
	{
		MatMul(&ST[(4*(l*l)*(l*l))*blockIdx.y + (l*l)*(l*l)*blockIdx.x], D, temp);
		AT[(l*l)*(l*l)*(blockIdx.x + (blockIdx.x/2) + 1) + (l*l)*threadIdx.x + blockIdx.y] = temp[threadIdx.x];
		__syncthreads();
	}

	else if(d_mv[2*blockIdx.y]>=0 && d_mv[2*blockIdx.y+1]<0)
	{
		MatMul(&ST[(4*(l*l)*(l*l))*blockIdx.y + (l*l)*(l*l)*blockIdx.x], D, temp);
		AT[(l*l)*(l*l)*(blockIdx.x + (blockIdx.x/2) + 3) + (l*l)*threadIdx.x + blockIdx.y] = temp[threadIdx.x];
		__syncthreads();
	}

	else
	{
		MatMul(&ST[(4*(l*l)*(l*l))*blockIdx.y + (l*l)*(l*l)*blockIdx.x], D, temp);
		AT[(l*l)*(l*l)*(blockIdx.x + (blockIdx.x/2) + 4) + (l*l)*threadIdx.x + blockIdx.y] = temp[threadIdx.x];
		__syncthreads();
	}

}


extern "C" 
void GetSystemMatrices(float *d_mv, float *d_A, float *d_AT)
{
    // Declare Local Data Variables
   	float *d_D, *d_D1, *d_S, *d_ST;
	float *d_tempStorage;
    float *h_Atemp, *h_S;
	
   	// Host Memory Allocation
   	h_S         = (float *) calloc((l*l)*4*(l*l)*(l*l),sizeof(float));
	h_Atemp     = (float *) calloc(9*(l*l)*(l*l),sizeof(float));
        
    // Device Memory Allocation
	cudaMalloc((void **)&d_D, (l*l)*sizeof( float));
	cudaMalloc((void **)&d_D1, (l*l)*(l*l)*sizeof( float));
	cudaMalloc((void **)&d_S, (l*l)*4*(l*l)*(l*l)*sizeof( float));
	cudaMalloc((void **)&d_ST, (l*l)*4*(l*l)*(l*l)*sizeof( float));
	cudaMalloc((void **)&d_tempStorage, (l*l)*16*(l*l)*(l*l)*sizeof( float));

   	cudaMemcpy(d_S, h_S, (l*l)*4*(l*l)*(l*l)*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_A, h_Atemp, 9*(l*l)*(l*l)*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_AT, h_Atemp, 9*(l*l)*(l*l)*sizeof(float), cudaMemcpyHostToDevice);

   	// Launch Kernels ...
    //           x     y
	dim3 blocks (16, (l*l));
	dim3 blocks1 (4, (l*l));
	dim3 threads((l*l), (l*l));
	
	kernel_GetShiftMatrices<<<1,threads>>>(d_D, d_D1, d_S, d_ST, d_mv);
	cudaThreadSynchronize();
	kernel_MatrixMulti<<<blocks,threads>>>(d_D1, d_S, d_ST, d_tempStorage);
	cudaThreadSynchronize();
	kernel_GetAMatrices<<<1,threads>>>(d_tempStorage, d_A);
	cudaThreadSynchronize();
	kernel_GetATMatrices<<<blocks1,(l*l)>>>(d_ST, d_D, d_AT, d_mv);
    cudaThreadSynchronize();

    // Free Memory of Local Data ...

    cudaFree(d_D);
    cudaFree(d_D1);
    cudaFree(d_S);
    cudaFree(d_ST);
    cudaFree(d_tempStorage);

    free(h_S);
    free(h_Atemp);

    
}