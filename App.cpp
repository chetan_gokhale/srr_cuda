#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <conio.h>
#include <cutil_inline.h>



#include "App.h"

void LoadLR(float *LR)
{
    // Loads (l*l) LR images each of size (n/l)*(n/l) to array LR

    char img[16][50] = {"TextCropped0.yuv","TextCropped1.yuv","TextCropped2.yuv","TextCropped3.yuv",
                        "TextCropped4.yuv","TextCropped5.yuv","TextCropped6.yuv","TextCropped7.yuv",
                        "TextCropped8.yuv","TextCropped9.yuv","TextCropped10.yuv","TextCropped11.yuv",
                        "TextCropped12.yuv","TextCropped13.yuv","TextCropped14.yuv","TextCropped15.yuv"};
    
    
    for(int i = 0; i<l*l; i++)
	{
		char path[50] = "LR\\NewText\\";
		const char* str = strcat(path,img[i]);
		LoadYUVImage(&LR[((n/l)*(n/l))*i], str, n/l, n/l);
	}

}

void WriteHR(float *HR)
{
    unsigned char *Arr;
    int Iindex, Jindex;
    
    Arr = (unsigned char *)calloc(n*n, sizeof(unsigned char));

	for(int i = 0 ; i<n/l; i++)
	    for(int j = 0; j<n/l; j++)
		    for(int k = 0;k<l*l; k++)
		    {
			    Iindex = i*l + (int)floor((double)k/l);
                Jindex = j*l + k%l;
                int temp = (int)HR[((n/l+2)*(l*l))*(i+1) + (l*l)*(j+1) + k];          //(int)(f[i+1][j+1][k] + 0.5)
                Arr[(n)*Iindex + Jindex] = temp;// + 30;
		    }
    StoreYUVImage(Arr, "Output.yuv", n, n);

}

int main()
{
    // Global Data Declaration
    float *h_LR, *h_f, *h_mv, *h_A;
    float *d_LR, *d_A, *d_AT, *d_f, *d_mv;

    // Host Memory Allocation
  	h_LR  = (float *) calloc((l*l)*(n/l)*(n/l), sizeof(float));
	h_f   = (float *) calloc((n+2*l)*(n+2*l), sizeof(float));
    h_mv  = (float *) calloc(2*(l*l), sizeof(float));
    h_A   = (float *) calloc(9*(l*l)*(l*l), sizeof(float));

    // Device Memory Allocation
	cudaMalloc((void **)&d_LR, (l*l)*(n/l)*(n/l)*sizeof( float));
	cudaMalloc((void **)&d_f, (n+2*l)*(n+2*l)*sizeof(float));
	cudaMalloc((void **)&d_A, 9*(l*l)*(l*l)*sizeof( float));
	cudaMalloc((void **)&d_AT, 9*(l*l)*(l*l)*sizeof( float));
	cudaMalloc((void **)&d_mv, 2*(l*l)*sizeof( float));

    // Initialize HR in Device
    cudaMemcpy(d_f, h_f, (n+2*l)*(n+2*l)*sizeof(float), cudaMemcpyHostToDevice);
    
    // Load LR images to Host and Device   
    LoadLR(h_LR);
    cudaMemcpy(d_LR, h_LR, (l*l)*(n/l)*(n/l)*sizeof(float), cudaMemcpyHostToDevice);
	
    GetMotionVectors(h_mv);         //(d_mv, d_LR)
	cudaMemcpy(d_mv, h_mv, 2*(l*l)*sizeof(float), cudaMemcpyHostToDevice);

    GetSystemMatrices(d_mv, d_A, d_AT);

   	/*cudaMemcpy(h_A, d_A, 9*(l*l)*(l*l)*sizeof(float), cudaMemcpyDeviceToHost);

	for(int i =0; i<(l*l); i++)
	{
		for(int j =0; j<(l*l); j++)
			printf("%f    ",h_A[(l*l)*(l*l)*4 + (l*l)*i + j]);
		printf("\n");
	}
	printf("\n");*/

    SRREngine(d_LR, d_f, h_f, d_A, d_AT);

    WriteHR(h_f);
    getch();

    return 0;
}